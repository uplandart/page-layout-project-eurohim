(function(document) {

	'use strict';

	if (!document.getElementsByClassName) {
		alert('Your browser is too old! Please update!');
		return;
	}

	var listTabs = document.getElementsByClassName('tabs');
	for (var i = 0; i < listTabs.length; i++) {
		var tabs = listTabs[i];
		var holderContentTabs = document.getElementById(tabs.getAttribute('tabs'));
		var liEls = tabs.getElementsByTagName('li');
		for (var j = 0; j < liEls.length; j++) {
			var liEl = liEls[j];
			liEl.tabs = tabs;
			liEl.holderContentTabs = holderContentTabs;
			var aEl = liEl.getElementsByTagName('a')[0];
			liEl.href = aEl.getAttribute('href').replace('#','');
			liEl.addEventListener('click', openTab, false);
		}
	}

	function openTab(event) {
		event.preventDefault ? event.preventDefault() : (event.returnValue = false);
		var sectionEl = document.getElementById(this.href);
		if (!sectionEl) return;
		if (sectionEl.classList.contains('active')) return;
		var allSectionsActive = this.holderContentTabs.querySelectorAll('section.content.active');
		var k;
		for (k = 0; k < allSectionsActive.length; k++) { allSectionsActive[k].classList.remove('active'); }
		var allLisActive = this.tabs.querySelectorAll('li.active');
		for (k = 0; k < allLisActive.length; k++) { allLisActive[k].classList.remove('active'); }
		sectionEl.classList.add('active');
		this.classList.add('active');
	}

})(document);